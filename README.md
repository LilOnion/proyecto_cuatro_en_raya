Reglas y Advertencias:

1) Se deben escoger columnas del uno al ocho.
2) El jugador vs CPU, tendrá ficha "X" y CPU "O".
3) Siempre, el jugador 1 sera ficha "X" y el jugador 2 "O".
4) El juego se acabará con un ganador cuando se arme una corrida de cuatro fichas identicas, ya sea vertical, horizontal o diagonal.
5) El juego se acabará en un empate si no quedan espacios por rellenar.
6) En la modalidad de juego, por favor escoger solo 1 o 2, o el juego no funcionará correctamente.

Para jugar de manera optima evitar las siguientes prácticas:
1) Ingresar fichas en columnas llenas.
2)Ingresar fichas a columnas que no existen.
3)Ingresar datos que no son acorde a lo que se pide en el juego.

Como esta creado:

El juego esta creado por varios procedimientos y funciones.
Entre ellos se encuentra definidas por "conceptos", es decir, que cada uno tiene una especialidad o un significado distinto,
por ejemplo, tiene funciones y procedimientos donde estan derivados especialmente a la identificación de quien gano, otras a rellenar
matriz, otra a imprimir esa matriz, e inluso una para "viajar por ella".
Ademas de un sistema de numero aleatorios para que el CPU o la maquina pueda realizar jugadas, sin necesidad de que un usuario
introduzca donde quire fijar la ficha.
El juego esta constituido por matrices, para un orden, sintaxis y mejor apilamiento de este juego, logrando una claridad en su contexto.
En este caso, se utilizo ciertos valores para las fichas, dandole un significado especial, es decir, que la ficha uno, al agregarle el valor 88, queda apreciado como una "X", y a la ficha dos, al agregarle el valor de 79, queda apreciado como un "O". Cabe decir que existe un numero vacio, el 32, utilizado en la matriz para dejar espacios, y con las fichas rellenarlos.